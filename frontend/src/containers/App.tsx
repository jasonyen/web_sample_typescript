import React, { lazy, Suspense, useEffect } from "react";
import { Switch, Route } from "react-router-dom";
import HttpServices from '@/services/HttpServices';

const App = () => {
    const httpServices = HttpServices();
    const callAPI = async () => {
        try {
            const config = {
                url: '/post/getPosts',
                method: 'get'
            };
            await httpServices.exec(config);
        } catch (error) {
            console.error('App error: ', error);
        }
    };

    useEffect(() => {
        callAPI();
    }, []);

    return (
        <>
            Welcome to web sample, api testing!
        </>
    );
};

export default App;