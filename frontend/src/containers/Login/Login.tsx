import React from 'react';
import HttpServices from '@/services/HttpServices';
import { History, LocationState } from 'history';

interface IProps {
    history: History<LocationState>
}

const Login = ({ history }: IProps) => {
    const httpServices = HttpServices();
    const onLoginClick = async () => {
        try {
            const login = {
                url: '/loginCallback',
                method: 'post',
                data: {
                    userId: 'E0001',
                    name: 'Jason',
                    email: 'jasonyen2008@gmail.com'
                }
            };
            let accessToken : string;
            accessToken = await httpServices.exec(login) as string;
            localStorage.setItem('accessToken', accessToken);
            history.push('app');
        } catch (error) {
            console.error('Login error: ', error);
        }
    };

    return (
        <>
            login
            <button onClick={onLoginClick}>Click to login</button>
        </>
    )
};

export default Login;