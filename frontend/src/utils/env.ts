// interface IConfig {
//     FRONTEND: string
// }
const defaultConfig = {
    API_URL: 'http://localhost:3000/api'
}
let FRONTEND : string = '';
export default FRONTEND ? JSON.parse(FRONTEND) : defaultConfig;
