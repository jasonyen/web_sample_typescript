import { useState } from 'react';
import Axios from 'axios';
import env from '@/utils/env';

interface IConfig {
    baseURL: string,
    withCredentials: boolean,
    headers: Object
};

const HttpServices = () => {
    const defaultConfig: IConfig = {
        baseURL: env.API_URL, // 以後會從 env 進來
        withCredentials: false,
        headers: {}
    };

    if (localStorage.accessToken) {
        defaultConfig.headers = { Authorization: localStorage.accessToken };
    }
    const axios = Axios.create(defaultConfig);
    // const [response, setResponse] = useState({});
    
    const exec = async (config = {}) => {
        let responseData = {};
        try {
            const { data } = await axios(config);
            responseData = data;
            // setResponse(responseData);
        } catch (e) {
            console.error('HttpServices error', e);
        } finally {
            return responseData;
        }
    };

    return ({
        exec,
    });
};

export default HttpServices;