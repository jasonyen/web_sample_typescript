export const presets = [
  "@babel/env",
  "@babel/preset-react"
];
export const plugins = [
  "@babel/plugin-transform-runtime"
];
  