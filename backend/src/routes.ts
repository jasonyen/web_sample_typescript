import express, { RequestHandler } from 'express';
import * as AuthController from './controllers/AuthController';
import * as PostController from './controllers/PostController';
import { isAuthorized } from './middlewares/Passport';
const routers = express.Router();

interface IRouteItem {
    path: string;
    method: 'get' | 'post' | 'put' | 'delete';
    middlewares: RequestHandler[];
};

const PREFIX = {
    POST: '/post'
};

export const AppRoutes: IRouteItem[] = [
    {
        path: `${PREFIX.POST}/getPosts`,
        method: 'get',
        middlewares: [
            isAuthorized,
            PostController.getPosts,
        ],
    },
    {
        path: `/loginCallback`,
        method: 'post',
        middlewares: [
            AuthController.loginCallback,
        ],
    },
];

// routers.get('/getPosts', PostController.getPosts);
// routers.post('/loginCallback', AuthController.loginCallback);
// routers.get('/verifyLogin', AuthController.verifyLogin);

// export default routers;
