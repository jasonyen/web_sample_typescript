import { Request, Response, NextFunction } from 'express';
import * as AuthService from '../services/AuthService';

export const loginCallback = (req: Request, res: Response, next: NextFunction) => {
    try {
        const { body } = req;
        const result = AuthService.loginCallback(body);
        return res.json(result);
    } catch (error) {
        next(error);
    }
};