import { Secret, sign } from 'jsonwebtoken';
import { WEB_SAMPLE_JWT_SECRET } from '../config/index';

export const loginCallback = (payload: any) => {
    const result = sign(
        {
            userId: payload.userId,
            name: payload.name,
            email: payload.email
        },
        WEB_SAMPLE_JWT_SECRET,
        { expiresIn: 3600 }   // 單位是秒
    );
    return result;
};