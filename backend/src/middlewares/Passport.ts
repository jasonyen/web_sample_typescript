import { Request, Response, NextFunction } from 'express';
import { Secret, verify } from 'jsonwebtoken';
import { WEB_SAMPLE_JWT_SECRET } from '../config/index';

interface IJwtDecode {
    userId: String,
    name: String,
    email: String
};

interface IJwtRequest extends Request {
    jwtDecode?: IJwtDecode
};

export const isAuthorized = (req: IJwtRequest, res: Response, next: NextFunction) => {
    const { authorization } = req.headers;
    if (authorization) {
        try {
            const decode = verify(authorization, WEB_SAMPLE_JWT_SECRET) as any;
            // console.log('****isAuthorized decode****', decode);
            if(decode.userId) {
                next();
            } else {
                throw new Error('INVALID_USER');
            }
        } catch (error) {
            // console.log('****isAuthorized error****', error);
            throw new Error('INVALID_USER');
        }
    } else {
        throw new Error('INVALID_USER');
    }
};